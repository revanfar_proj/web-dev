$(function () {

    Morris.Line({
        element: 'cpu-chart',
        data: [
            {timestamp: '2017-02-04 00:00:00', a: 100, b: 90},
            {timestamp: '2017-02-04 00:30:00', a: 75, b: 85},
            {timestamp: '2017-02-04 01:00:00', a: 50, b: 30},
            {timestamp: '2017-02-04 01:30:00', a: 75, b: 85},
            {timestamp: '2017-02-04 02:00:00', a: 50, b: 50},
            {timestamp: '2017-02-04 02:30:00', a: 75, b: 35},
            {timestamp: '2017-02-04 03:00:00', a: 100, b: 90}
        ],
        xkey: 'timestamp',
        xLabels: '30min',
        ykeys: ['a', 'b'],
        ymax: 100,
        postUnits: '%',
        lineColors: ['red', 'blue'],
        labels: ['CPU 1', 'CPU2'],
    });
    
    Morris.Line({
        element: 'memory-chart',
        data: [
            {y: '06:00', a: 9},
            {y: '06:30', a: 35},
            {y: '07:00', a: 20},
            {y: '07:30', a: 65},
            {y: '08:00', a: 43},
            {y: '08:30', a: 22},
            {y: '09:00', a: 10}
        ],
        xkey: 'y',
        ykeys: ['a'],
        labels: ['Series A'],
        ymax: 100,
        postUnits: '%',
        parseTime: false
    });
    
    Morris.Line({
        element: 'eth0-chart',
        data: [
            {y: '06:00', b: 33},
            {y: '06:30', b: 27},
            {y: '07:00', b: 55},
            {y: '07:30', b: 44},
            {y: '08:00', b: 7},
            {y: '08:30', b: 10},
            {y: '09:00', b: 20}
        ],
        xkey: 'y',
        ykeys: ['b'],
        labels: ['Series A'],
        postUnits: ' kbps',
        parseTime: false
    });
    
    Morris.Line({
        element: 'eth1-chart',
        data: [
            {y: '06:00', b: 13},
            {y: '06:30', b: 17},
            {y: '07:00', b: 21},
            {y: '07:30', b: 20},
            {y: '08:00', b: 23},
            {y: '08:30', b: 23},
            {y: '09:00', b: 29}
        ],
        xkey: 'y',
        ykeys: ['b'],
        labels: ['Series A'],
        postUnits: ' kbps',
        parseTime: false
    });
});
