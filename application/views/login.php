<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>vCPE | <?php echo ucfirst($title) ?></title>
        <link href="<?php echo assets_url() ?>css/main.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>dist/css/sb-admin-2.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/morrisjs/morris.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="<?php echo assets_url() ?>vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/metisMenu/metisMenu.min.js"></script>
        <script src="<?php echo assets_url() ?>dist/js/sb-admin-2.js"></script>
        <script src="<?php echo assets_url() ?>vendor/raphael/raphael.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap-toggle/bootstrap-toggle.min.js"></script>    
        <script src="<?php echo assets_url() ?>vendor/chart/Chart.min.js"></script>    
    </head>
    <body style="background: #C9C9C9">
        <div id="wrapper">
            <div class="col-lg-offset-4 col-lg-4 col-sm-12" style="margin-top: 15%;">
                <div id="customer" class="panel">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <img src="<?php echo assets_url() ?>img/logo.png" height="30"/>
                        </div>
                        <div class="col-sm-12">
                            <hr/>
                        </div>
                        <div class="col-sm-12">
                            <form class="form" action="<?php echo site_url('main/authentication') ?>" method="post" enctype="application/x-www-form-urlencoded">
                                <div class="form-group">
                                    <label>Customer ID</label>
                                    <input class="form-control" type="text" name="username"/>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password"/>
                                    <input type="hidden" name="group" value="customer"/>
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-info pull-right" type="submit" value="Sign In"/>
                                    <a class="btn btn-info pull-right" style="margin-right: 4px;" onclick="admin()">Admin Login</a>
                                    <a class="btn btn-info pull-right" style="margin-right: 4px;" href="<?php echo site_url('main/order') ?>">Order Device</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="loader" class="panel">
                    <div class="panel-body" style="padding: 160px; padding-top: 92px; padding-bottom: 91px; ">
                        <div class="loader" id="loader" style="width: 100px; height: 100px"></div>
                    </div>
                </div>
                <div id="admin" class="panel">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <img src="<?php echo assets_url() ?>img/logo.png" height="30"/>
                        </div>
                        <div class="col-sm-12">
                            <hr/>
                        </div>
                        <div class="col-sm-12">
                            <form class="form" action="<?php echo site_url('main/authentication') ?>" method="post" enctype="application/x-www-form-urlencoded">
                                <div class="form-group">
                                    <label>Username</label>
                                    <input class="form-control" type="text" name="username"/>
                                </div>
                                <div class="form-group">
                                    <label>Password</label>
                                    <input class="form-control" type="password" name="password"/>
                                    <input type="hidden" name="group" value="admin"/>
                                </div>
                                <div class="form-group">
                                    <input class="btn btn-info pull-right" type="submit" value="Sign In"/>
                                    <a class="btn btn-info pull-right" style="margin-right: 4px;" onclick="customer()">Customer Login</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script>
        $(document).ready(function () {
            $('#admin').hide();
            $('#loader').hide();
            $('#customer').show();
        });


        function admin() {
            $('#customer').hide();
            $('#loader').show();
            setTimeout(function () {
                $('#loader').hide();
                $('#admin').show();
            }, 500);
        }

        function customer() {
            $('#admin').hide();
            $('#loader').show();
            setTimeout(function () {
                $('#loader').hide();
                $('#customer').show();
            }, 500);
        }
    </script>
</html>