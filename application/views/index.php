<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>vCPE | <?php echo ucfirst($title) ?></title>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <link href="<?php echo assets_url() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/morrisjs/morris.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url() ?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url() ?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url() ?>dist/css/sb-admin-2.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>css/main.css" rel="stylesheet">
        <script src="<?php echo assets_url() ?>vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/jquery-validation/dist/additional-methods.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootbox/bootbox.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/flot/jquery.flot.js"></script>
        <script src="<?php echo assets_url() ?>vendor/flot/jquery.flot.resize.js"></script>
        <script src="<?php echo assets_url() ?>vendor/flot/jquery.flot.time.js"></script>
        <script src="<?php echo assets_url() ?>vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/datatables-responsive/dataTables.responsive.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/metisMenu/metisMenu.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/raphael/raphael.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap-toggle/bootstrap-toggle.min.js"></script>    
        <script src="<?php echo assets_url() ?>vendor/chart/Chart.min.js"></script>
    </head>
    <body>
        <div id="wrapper">
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="<?php echo assets_url() ?>img/logo.png" height="20px"/></a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <a href="#" class="submenu">
                        <i class="fa fa-user fa-fw"></i><?php echo $this->session->userdata('name'); ?>
                    </a>
                    <a href="<?php echo site_url('main/logout') ?>" class="submenu">
                        <i class="fa fa-sign-out"></i> Sign Out
                    </a>
                </ul>
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse">
                        <?php
                        if ($this->session->userdata('username') != 'admin') {
                            ?>
                            <ul class="nav" id="side-menu">
                                <li>
                                    <a href="<?php echo site_url('') ?>"><i class="fa fa-home fa-fw"></i> Dashboard</a>
                                </li>
                            </ul>
                            <?php
                        } else {
                            ?>
                            <ul class="nav" id="side-menu">
                                <li>
                                    <a href="<?php echo site_url('') ?>"><i class="fa fa-server fa-fw"></i> Dashboard</a>
                                </li>
                            </ul>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </nav>
            <div id="page-wrapper">
                <?php $this->load->view('pages/' . $page); ?>
            </div>
        </div>
    </body>
    <script src="<?php echo assets_url() ?>dist/js/sb-admin-2.js"></script>
</html>
