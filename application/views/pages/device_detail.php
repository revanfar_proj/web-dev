<?php
$cpu = 0;
$ram_max = 1;
$ram = 0;
$storage = 0;
$storage_max = 1;
$latest = array();
$config = array();
$device_id = $this->uri->segment(3);
$configpath = device_url() . $device_id . "/conf_example.json";
if (file_exists('/var/www/html/data/vcpe/' . $device_id . '/conf_example.json')) {
    $config = json_decode(file_get_contents($configpath), true);
}
$statpath = device_url() . $device_id . "/stats";

if (file_exists('/var/www/html/data/vcpe/' . $device_id . '/stats')) {
    $stat = json_decode(file_get_contents($statpath), true);
    if (count($stat) > 0) {
        $latest = end($stat);
        date_default_timezone_set('UTC');
        $cpu = floor(($latest["host"]["cpu"]["core0"] + $latest["host"]["cpu"]["core1"]) / 2);
        $ram_max = $latest["host"]["ram"]["max"] / (1024 * 1024);
        $ram = $ram_max * $latest["host"]["ram"]["percent"] / 100;
        $storage = $latest["host"]["disk"]["usage"] / 1000000;
        $storage_max = $latest["host"]["disk"]["max"] / 1000000;
        /*
          if ($latest["vRouter"]["state"] == 1) {
          $cpu++;
          }
          if ($latest["vFirewall"]["state"] == 1) {
          $cpu++;
          } */
    }
}
?>
<br/>
<div class="row">
    <ul class="nav navbar-top-links navbar-left">
        <a class="submenu" data-toggle="tab" href="#status">Status</a>
        <?php
        if (array_key_exists('vRouter', $config)) {
            if ($config["vRouter"] == "on") {
                ?>
                <a class="submenu" data-toggle="tab" href="#address">Addressing and DHCP</a>
                <a class="submenu" data-toggle="tab" href="#routes">Routing</a>
                <a class="submenu" data-toggle="tab" href="#nat" hidden>NAT</a>
                <?php
            }
        }
        ?>
    </ul>
    <br>
    <hr/>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile overflow_hidden">
            <br>
            <div class="tab-content">
                <div id="status" class="tab-pane fade in active">
                    <div class="col-sm-12" style="padding: 0;">
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <div class="row">
                                    <div>
                                        <div class="col-sm-11"><i class="fa fa-desktop"></i>&nbsp; <span id="vcpe-title">vCPE</span></div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <canvas id="cpu-pie"></canvas>
                                        <p align="center" style="font-size: 11px;"><b>CPU<br>Used <?php echo $cpu ?>% of 100%</b><br>
                                        </p>
                                    </div>  
                                    <div class="col-sm-3">
                                        <canvas id="ram-pie"></canvas>
                                        <p align="center" style="font-size: 11px;"><b>RAM<br>Used <?php echo number_format(ceil($ram)) ?> MB of <?php echo number_format(ceil($ram_max)) ?> MB</b><br>
                                    </div>  
                                    <div class="col-sm-3">
                                        <canvas id="storage-pie"></canvas>
                                        <p align="center" style="font-size: 11px;"><b>Volume Storage<br>Used <?php echo number_format(ceil($storage)) ?> MB  of <?php echo number_format(ceil($storage_max)) ?> MB</b><br>
                                    </div>
                                    <div class="col-sm-12">
                                        <br/>
                                        <table class="table table-responsive table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th colspan="6">vCPE Information</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>vRouter</th><td id="vrouter_state"><span><input id="vRouter" data-on="Enabled" data-off="Disabled" type="checkbox" data-toggle="toggle" data-size="mini"
                                                            <?php
                                                            if (array_key_exists('vRouter', $config)) {
                                                                if ($config["vRouter"] == "on") {
                                                                    echo "checked";
                                                                }
                                                            }
                                                            ?>/></span></td>
                                                    <th>vFirewall</th><td id="vfirewall_state"><span><input id="vFirewall" data-on="Enabled" data-off="Disabled" type="checkbox" data-toggle="toggle" data-size="mini"
                                                            <?php
                                                            if (array_key_exists('vFirewall', $config)) {
                                                                if ($config["vFirewall"] == "on") {
                                                                    echo "checked";
                                                                }
                                                            }
                                                            ?>/></span></td>
                                                    <th>Latest Update</th><td><?php
                                                        if (array_key_exists('datetime', $latest)) {
                                                            echo date("Y/m/d H:i", strtotime($latest["datetime"]));
                                                        }
                                                        ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Management IP</th><td id="mgmt_ip">offline</td>
                                                    <th>Start Connection</th><td id="start_conn">unknown</td>
                                                    <th>Last Connection</th><td id="last_ref">unknown</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Resource Usage Graph
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="flot-chart">
                                                    <div class="flot-chart-content" id="utility-graph"></div>
                                                </div>
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Network Usage Graph
                                            </div>
                                            <!-- /.panel-heading -->
                                            <div class="panel-body">
                                                <div class="flot-chart">
                                                    <div class="flot-chart-content" id="net-graph"></div>
                                                </div>
                                            </div>
                                            <!-- /.panel-body -->
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="address" class="tab-pane fade">
                    <div class="col-lg-12" style="padding: 0;">
                        <div class="col-lg-6">
                            <p class="header">Interface</p>
                            <hr/>
                        </div>
                        <div class="col-lg-6">
                            <p class="header">NAT</p>
                            <hr/>
                        </div>
                        <div class="col-lg-2">
                            <b>Interface Setting</b>
                            <p>Use this to change IP Address for WAN Interface.</p>
                        </div>
                        <div class="col-lg-4">
                            <form id="interface">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" id="service" name="service" value="interface"/>
                                    <p>WAN Interface</p>
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0;">
                                        <input class="form-control" type="text" id="wan" name="wan" value="<?php
                                        if (array_key_exists("wan_interface", $config)) {
                                            echo $config["wan_interface"];
                                        }
                                        ?>"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">   
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0; padding-top: 5px">
                                        <a class="btn btn-primary pull-right" onclick="configUpdate('interface')">Submit</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-lg-1">
                            <b>Src. NAT</b>
                        </div>
                        <div class="col-lg-5">
                            <span><input id="src_nat" data-on="Enabled" data-off="Disabled" type="checkbox" data-toggle="toggle" data-width="100px" data-size="mini"
                                <?php
                                if (array_key_exists('src_nat', $config)) {
                                    if ($config["src_nat"] == "on") {
                                        echo "checked";
                                    }
                                }
                                ?>/></span><p>This toggle button will enable Source NAT config. <br/>(Source Address: 192.168.123.0/29, Translation Address: Masquerade</p>
                        </div>
                    </div>
                    <div class="col-lg-12" style="padding: 0;">
                        <hr/>
                        <p class="header">DHCP</p>
                        <hr/>
                        <div class="col-lg-2">
                            <b>DHCP Configuration</b>
                            <p>Fill this form to create a DHCP Server for your network's.</p>
                        </div>
                        <div class="col-lg-4">
                            <form id="dhcp">
                                <div class="form-group">
                                    <input class="form-control" type="hidden" id="service" name="service" value="dhcp"/>
                                    <p>Subnet</p>
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0;">
                                        <input class="form-control" type="text" id="subnet" name="subnet" value="<?php
                                        if (array_key_exists("dhcp", $config)) {
                                            if (array_key_exists("ip_subnet", $config["dhcp"])) {
                                                echo $config["dhcp"]["ip_subnet"];
                                            }
                                        }
                                        ?>"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <p>IP Start</p>
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0;">
                                        <input class="form-control" type="text" id="start" name="start" value="<?php
                                        if (array_key_exists("dhcp", $config)) {
                                            if (array_key_exists("ip_start", $config["dhcp"])) {
                                                echo $config["dhcp"]["ip_start"];
                                            }
                                        }
                                        ?>"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <p>IP Stop</p>
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0;">
                                        <input class="form-control" type="text" id="stop" name="stop" value="<?php
                                        if (array_key_exists("dhcp", $config)) {
                                            if (array_key_exists("ip_stop", $config["dhcp"])) {
                                                echo $config["dhcp"]["ip_stop"];
                                            }
                                        }
                                        ?>"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <p>Gateway</p>
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0;">
                                        <input class="form-control" type="text" id="gateway" name="gateway" value="<?php
                                        if (array_key_exists("dhcp", $config)) {
                                            if (array_key_exists("gateway", $config["dhcp"])) {
                                                echo $config["dhcp"]["gateway"];
                                            }
                                        }
                                        ?>"/>
                                    </div>
                                </div>
                                <br/>
                                <div class="form-group">
                                    <p>Lease</p>
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0;">
                                        <input class="form-control" type="number" id="lease" name="lease" value="<?php
                                        if (array_key_exists("dhcp", $config)) {
                                            if (array_key_exists("lease", $config["dhcp"])) {
                                                echo $config["dhcp"]["lease"];
                                            }
                                        }
                                        ?>"/>
                                    </div>
                                </div>  
                                <div class="form-group">   
                                    <div class="col-lg-12" style="padding-left: 0; padding-right: 0; padding-top: 5px">
                                        <a class="btn btn-primary pull-right" onclick="configUpdate('dhcp')">Submit</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="routes" class="tab-pane fade">
                    <div class="col-lg-12" style="padding: 0;">
                        <p class="header">Static Route</p>
                        <hr/>
                        <div class="col-lg-2">
                            <b>Routes</b>
                        </div>
                        <div class="col-lg-10">
                            <table id="staticTables" class="table table-hover table-striped" style="min-width: 400px; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Subnet</th>
                                        <th>Next-Hop</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (array_key_exists('static', $config)) {
                                        foreach ($config["static"] as $row) {
                                            ?>
                                            <tr>
                                                <td style="padding-top: 13px; width: 150px;"><?php echo $row["network"]; ?></td>
                                                <td style="padding-top: 13px; width: 350px;"><?php echo $row["nexthop"]; ?></td>
                                                <td style="padding-top: 13px;"><a href="#" class="fa fa-remove pull-right" id="delete" onclick="configDelete('static')"></a></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-12" style="padding: 0;">
                        <hr/>
                        <p class="header">OSPF</p>
                        <hr/>
                        <div class="col-lg-2">
                            <b>Routes</b>
                        </div>
                        <div class="col-lg-10">
                            <table id="ospfTables" class="table table-hover table-striped" style="min-width: 400px; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Network</th>
                                        <th>Area</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (array_key_exists('ospf', $config)) {
                                        foreach ($config["ospf"] as $row) {
                                            ?>
                                            <tr>
                                                <td style="padding-top: 13px; width: 150px;"><?php echo $row["network"]; ?></td>
                                                <td style="padding-top: 13px; width: 350px;"><?php echo $row["area"]; ?></td>
                                                <td style="padding-top: 13px;"><a href="#" class="fa fa-remove pull-right" id="delete" onclick="configDelete('ospf')"></a></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div id="nat" class="tab-pane fade">
                    <div class="col-lg-12" style="padding: 0;">
                        <p class="header">Source NAT</p>
                        <hr/>
                        <div class="col-lg-2">
                            Source NAT Rules Table
                        </div>
                        <div class="col-lg-10">
                            <table class="table table-striped table-hover" id="snatTables">
                                <thead>
                                    <tr>
                                        <th>Rule Number</th>
                                        <th>Src. Addr</th>
                                        <th>Src. Port</th>
                                        <th>Dst. Addr</th>
                                        <th>Dst. Port</th>
                                        <th>Trans. Addr</th>
                                        <th>Trans. Port</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (array_key_exists('src_nat', $config)) {
                                        foreach ($config["src_nat"] as $row) {
                                            ?>
                                            <tr>
                                                <td style="padding-top: 13px;"><?php echo $row["rule"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["src_address"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["src_port"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["dst_address"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["dst_port"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["translate_address"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["translate_port"]; ?></td>
                                                <td style="padding-top: 13px;"><a href="#" class="fa fa-remove pull-right" id="delete" onclick="configDelete('src_nat')"></a></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <a data-toggle="modal" href="#form_snatrules"><i class="fa fa-plus"> Add Source NAT Rule</i></a>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <p class="header">Destination NAT</p>
                        <hr/>
                        <div class="col-lg-2">
                            Destination NAT Rules Table
                        </div>
                        <div class="col-lg-10">
                            <table class="table table-striped table-hover" id="dnatTables">
                                <thead>
                                    <tr>
                                        <th>Rule Number</th>
                                        <th>Src. Addr</th>
                                        <th>Src. Port</th>
                                        <th>Dst. Addr</th>
                                        <th>Dst. Port</th>
                                        <th>Trans. Addr</th>
                                        <th>Trans. Port</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (array_key_exists('dst_nat', $config)) {
                                        foreach ($config["dst_nat"] as $row) {
                                            ?>
                                            <tr>
                                                <td style="padding-top: 13px;"><?php echo $row["rule"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["src_address"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["src_port"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["dst_address"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["dst_port"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["translate_address"]; ?></td>
                                                <td style="padding-top: 13px;"><?php echo $row["translate_port"]; ?></td>
                                                <td style="padding-top: 13px;"><a href="#" class="fa fa-remove pull-right" id="delete" onclick="configDelete('dst_nat')"></a></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <a data-toggle="modal" href="#form_dnatrules"><i class="fa fa-plus"> Add Destination NAT Rule</i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--FORM MODAL-->
<div id="form_dns" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DNS Forwarder</h4>
            </div>  
            <div class="modal-body">
                <form id="dns_form" class="form-horizontal" method="post" action="">
                    <div class="form-group">
                        <div class="col-xs-12">
                            <label>Nameserver 1</label>
                            <input class="form-control" id="name-server[0]" name="name-server[0]" type="text" value="<?php echo $config["dns"]["nameserver1"] ?>"/>
                        </div>
                        <div class="col-xs-12">
                            <label>Nameserver 2</label>
                            <input class="form-control" id="name-server[1]" name="name-server[1]" type="text" value="<?php echo $config["dns"]["nameserver2"] ?>"/>
                        </div>
                        <div class="col-xs-12">
                            <input class="form-control" id="service" name="service" type="hidden" value="dns"/>
                        </div>
                    </div>
                </form>
                <button class="btn btn-primary" onclick="configAdd('dns_form')">Save</button>
            </div>
        </div>
    </div>
</div>

<div id="form_vpn" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">DNS Forwarder</h4>
            </div>  
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="#">
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Username</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="username" name="username" type="text"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Password</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="password" name="password" type="text"/>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-md-offset-10 col-md-2">
                            <button type="submit" id="submit" class="btn btn-info pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_riprules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="#">
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Network</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="network" name="network" type="text" placeholder="eg:192.168.0.0/24"/>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-md-offset-10 col-md-2">
                            <button type="submit" id="submit" class="btn btn-primary pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_staticrules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form id="static" class="form-horizontal" method="post" action="#">
                    <div class="form-group">
                        <input class="form-control" id="service" name="service" type="hidden" value="static"/>
                        <div class="col-xs-2">
                            <label class="control-label">Network</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="network" name="network" type="text" placeholder="eg:192.168.0.0/24"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label class="control-label">Next-Hop</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="nexthop" name="nexthop" type="text" placeholder="eg:192.168.0.0"/>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-12">
                            <a class="btn btn-primary pull-right" onclick="configAdd('static', 'form_staticrules')">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_snatrules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post">
                    <div class="form-group">
                        <input class="form-control" id="service" name="service" type="hidden" value="src_nat"/>
                        <div class="col-xs-2">
                            <label>Rule Number</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="rule" name="rule" type="number" min="1" max="9999"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Source Address</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="source_addr" name="source_addr" type="text" placeholder="eg: &quot;192.168.0.0/24&quot;"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Source Port</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="source_port" name="source_port" type="number" min="1" max="65535"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Destination Address</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="destination" name="destination_addr" type="text" placeholder="eg: &quot;192.168.0.0/24&quot;"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Destination Port</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="destination_port" name="destination_port" type="number" min="1" max="65535"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Translation Address</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="translation_addr" name="translation_addr" type="text" placeholder="eg: &quot;192.168.0.1&quot;, &quot;192.168.0.0/24&quot; or masquerade"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Translation Port</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="translation_port" name="translation_port" type="number" min="1" max="65535"/>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-12">
                            <a class="btn btn-primary pull-right" onclick="configAdd('src_nat', 'form_snatrules')">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_dnatrules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form id="dst_nat" class="form-horizontal" method="post" action="#">
                    <input class="form-control" id="service" name="service" type="hidden" value="dst_nat"/>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Rule Number</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="rule" name="rule" type="number" min="1" max="9999"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Source Address</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="source" name="source_addr" type="text" placeholder="eg: &quot;192.168.0.0/24&quot;"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Source Port</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="source_port" name="source_port" type="number" min="1" max="65535"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Destination Address</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="destination" name="destination_addr" type="text" placeholder="eg: &quot;192.168.0.0/24&quot;"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Destination Port</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="destination_port" name="destination_port" type="number" min="1" max="65535"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Translation Address</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="translation_addr" name="translation_addr" type="text" placeholder="eg: &quot;192.168.0.1&quot;, &quot;192.168.0.0/24&quot; or masquerade"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Translation Port</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="translation_port" name="translation_port" type="number" min="1" max="65535"/>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-12">
                            <a class="btn btn-primary pull-right" onclick="configAdd('dst_nat', 'form_dnatrules')">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_ospfrules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form id="ospf" class="form-horizontal" method="post" action="#">
                    <input class="form-control" id="service" name="service" type="hidden" value="ospf"/>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label class="control-label">Network</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="network" name="network" type="text" placeholder="eg:192.168.0.0/24" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label class="control-label">Area</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="area" name="area" min="0" max="1000000" type="number" required/>
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-12">
                            <a class="btn btn-primary pull-right" onclick="configAdd('ospf', 'form_ospfrules')">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_ipblock" class="modal fade" tabindex="-1" data-focus-on="input:first">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">IP Filtering</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Source</th>
                            <th>Destination</th>
                            <th>Action</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="6">No Rules Yet</td>
                        </tr>
                    </tbody>
                </table>
                <button class="btn btn-default" data-toggle="modal" href="#form_iprules">Create New Rules</button>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="form_iprules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="#">
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Interace</label>
                        </div>
                        <div class="col-xs-10">
                            <select class="selectpicker" name="interfaces[]" multiple>
                                <optgroup label="Interfaces">
                                    <option>LAN</option>
                                    <option>WAN</option>
                                </optgroup>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Source</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="source" name="source" type="text" placeholder="eg: 192.168.0.1 or 192.168.0.0/24 or any"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Destination</label>
                        </div>
                        <div class="col-xs-10">
                            <input class="form-control" id="destination" name="destination" type="text" placeholder="eg: 192.168.0.1 or 192.168.0.0/24 or any"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Action</label>
                        </div>
                        <div class="col-xs-10">
                            <select class="selectpicker" name="action">
                                <optgroup label="Action">
                                    <option>Pass</option>
                                    <option>Block</option>
                                    <option>Reject</option>
                                </optgroup>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-md-offset-10 col-md-2">
                            <button type="submit" id="submit" class="btn btn-primary pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="form_portblock" class="modal fade" tabindex="-1" data-focus-on="input:first">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Port Blocking</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Port</th>
                            <th>Action</th>
                            <th>Description</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5">No Rules Yet</td>
                        </tr>
                    </tbody>
                </table>
                <button class="btn btn-default" data-toggle="modal" href="#form_portrules">Create New Rules</button>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="form_portrules" class="modal fade" tabindex="-1" data-focus-on="input:first" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Create Rules</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" method="post" action="#">
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Interace</label>
                        </div>
                        <div class="col-xs-10">
                            <select class="selectpicker" name="interfaces[]" multiple>
                                <optgroup label="Interfaces">
                                    <option>LAN</option>
                                    <option>WAN</option>
                                </optgroup>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Port Number</label>
                        </div>
                        <div class="col-xs-5">
                            <input class="form-control" id="source" name="source" type="number" min="1"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-2">
                            <label>Action</label>
                        </div>
                        <div class="col-xs-10">
                            <select class="selectpicker" name="action">
                                <optgroup label="Action">
                                    <option>Pass</option>
                                    <option>Block</option>
                                    <option>Reject</option>
                                </optgroup>
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-md-offset-10 col-md-2">
                            <button type="submit" id="submit" class="btn btn-primary pull-right">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="alertbox" class="modal fade" data-backdrop="static" style="display: none; top: 40%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="alertmessage" class="modal-title" style="text-align: center">Device Register Form</h5>
            </div>
        </div>
    </div>
</div>
<!--/FORM MODAL-->
<script>
    var date = new Date;
    var tableData = [];

    $(function () {
        $('#vRouter').change(function () {
            vnfStatus($(this).attr("id"), $(this).prop('checked'));
        });
        $('#vFirewall').change(function () {
            vnfStatus($(this).attr("id"), $(this).prop('checked'));
        });
        $('#src_nat').change(function () {
            vnfStatus($(this).attr("id"), $(this).prop('checked'));
        });
    });

    $(document).ready(function () {
        $('#staticTables').DataTable({
            responsive: true,
            pageLength: 5,
            bLengthChange: false,
            language: {
                info: "<a href=\"\" data-toggle=\"modal\" data-target=\"#form_staticrules\"><i class=\"fa fa-plus\"> Add a Static Route</i></a>",
                infoEmpty: "<a href=\"\" data-toggle=\"modal\" data-target=\"#form_staticrules\"><i class=\"fa fa-plus\"> Add a Static Route</i></a>",
                infoFiltered: "<a href=\"\" data-toggle=\"modal\" data-target=\"#form_staticrules\"><i class=\"fa fa-plus\"> Add a Static Route</i></a>"
            }
        });
        $('#ospfTables').DataTable({
            responsive: true,
            pageLength: 5,
            bLengthChange: false,
            language: {
                info: "<a href=\"\" data-toggle=\"modal\" data-target=\"#form_ospfrules\"><i class=\"fa fa-plus\"> Add an OSPF Route</i></a>",
                infoEmpty: "<a href=\"\" data-toggle=\"modal\" data-target=\"#form_ospfrules\"><i class=\"fa fa-plus\"> Add an OSPF Route</i></a>",
                infoFiltered: "<a href=\"\" data-toggle=\"modal\" data-target=\"#form_ospfrules\"><i class=\"fa fa-plus\"> Add an OSPF Route</i></a>"
            }
        });

        $.ajax({
            type: "GET",
            url: "<?php echo base_url("api/openvpn/" . $this->uri->segment(3)); ?>",
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.result) {
                    $("#mgmt_ip").html(obj.vpn_ip);
                    $("#start_conn").html(obj.conn_start);
                    $("#last_ref").html(obj.last_ref);
                } else {
                    $("#vrouter_state").html('unknown');
                    $("#vfirewall_state").html('unknown');
                    $("#vcpe-title").html('vCPE (Offline)');
                }
            }, error: function (data) {
                alert("failed detect device management agent. device maybe offline or shutdown.");
            }
        });

        $("tr td #delete").click(function () {
            tableData = $(this).closest("tr").children("td").map(function () {
                return $(this).text();
            }).get();
        });

        jQuery.validator.addMethod('ipv4', function (value) {
            var split = value.split('.');
            if (split.length != 4)
                return false;

            for (var i = 0; i < split.length; i++) {
                var s = split[i];
                if (s.length == 0 || isNaN(s) || s < 0 || s > 255)
                    return false;
            }
            return true;
        }, 'Invalid IP address');

        jQuery.validator.addMethod('subnetv4', function (value) {
            var network = value.split('/');
            if (network.length != 2)
                return false;

            var subnet = network[1];
            if (isNaN(subnet) || subnet < 0 || subnet > 32)
                return false;

            var ip = network[0].split('.');
            for (var i = 0; i < ip.length; i++) {
                var s = ip[i];
                if (s.length == 0 || isNaN(s) || s < 0 || s > 255)
                    return false;
            }
            return true;
        }, 'Invalid Network Address');

        $('#static').validate({
            rules: {
                network: {
                    subnetv4: true,
                    required: true
                },
                nexthop: {
                    ipv4: true,
                    required: true
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#ripTables").DataTable({
            responsive: true,
            columnDefs: [
                {"orderable": false, "targets": 2}
            ]
        });

        $('#dhcp').validate({
            rules: {
                subnet: {
                    subnetv4: true,
                },
                start: {
                    ipv4: true,
                },
                stop: {
                    ipv4: true,
                },
                gateway: {
                    ipv4: true,
                },
                lease: {
                    required: true,
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
        
        $('#interface').validate({
            rules: {
                wan: {
                    subnetv4: true,
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#src_nat').validate({
            rules: {
                source: {
                    required: true
                },
                destination: {
                    required: true
                },
                translation: {
                    required: true
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#dst_nat').validate({
            rules: {
                source: {
                    required: true
                },
                destination: {
                    required: true
                },
                translation: {
                    required: true
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $("#ripTables").DataTable({
            responsive: true,
            columnDefs: [
                {"orderable": false, "targets": 2}
            ]
        });

        plot();

        function plot() {

            $.getJSON('<?php echo device_url(); ?><?php echo $this->uri->segment(3) ?>/stats', function (json) {
                var recv = [];
                var sent = [];
                var core0 = [];
                var core1 = [];
                $.each(json, function (index, element) {
                    var datetime = element.datetime;
                    datetime = datetime.split("-");
                    var time = datetime[1].match(/.{1,2}/g);
                    var date = datetime[0].match(/.{1,4}/g);
                    date[1] = date[1].match(/.{1,2}/g);
                    if (typeof element.host.net.enp2s0 != "undefined") {
                        var data = [Date.UTC(date[0], parseInt(date[1][0]) - 1, date[1][1], time[0], time[1], 0), element.host.net.enp2s0.recv / 1000];
                        recv.push(data);
                        var data = [Date.UTC(date[0], parseInt(date[1][0]) - 1, date[1][1], time[0], time[1], 0), element.host.net.enp2s0.sent / 1000];
                        sent.push(data);
                    }
                    var data = [Date.UTC(date[0], parseInt(date[1][0]) - 1, date[1][1], time[0], time[1], 0), element.host.cpu.core0];
                    core0.push(data);
                    var data = [Date.UTC(date[0], parseInt(date[1][0]) - 1, date[1][1], time[0], time[1], 0), element.host.cpu.core1];
                    core1.push(data);
                });
                var options = {
                    yaxis: {
                        min: 0,
                        max: 100,
                        tickFormatter: function (v, axis) {
                            return v + " MB/s";
                        }
                    },
                    xaxis: {
                        mode: "time",
                        timezone: "Asia/Jakarta",
                        min: date.getTime() + 60 * 60000,
                        max: date.getTime() + 420 * 60000,
                        tickSize: [30, "minute"]
                    },
                    series: {
                        lines: {
                            show: true,
                            fill: true
                        }
                    },
                    clickable: true,
                    hoverable: true,
                    colors: ["#00a9e0", "#617bff"]
                };

                var plotObj = $.plot($("#net-graph"), [{
                        data: sent,
                        label: "Receive Bytes"
                    }, {
                        data: recv,
                        label: "Sent Bytes"
                    }],
                        options
                        );

                var options = {
                    yaxis: {
                        min: 0,
                        max: 100,
                        tickFormatter: function (v, axis) {
                            return v + " %";
                        }
                    },
                    xaxis: {
                        mode: "time",
                        timezone: "Asia/Jakarta",
                        min: date.getTime() + 60 * 60000,
                        max: date.getTime() + 420 * 60000,
                        tickSize: [30, "minute"]
                    },
                    series: {
                        lines: {
                            show: true,
                            fill: true
                        }
                    },
                    clickable: true,
                    hoverable: true,
                    colors: ["#00a9e0", "#617bff"]
                };

                var plotObj = $.plot($("#utility-graph"), [{
                        data: core0,
                        label: "Core-0"
                    }, {
                        data: core1,
                        label: "Core-1"
                    }],
                        options
                        );
            });

        }

        var ctx = document.getElementById("cpu-pie").getContext("2d");

        var data = {
            labels: [
                "Used",
                "Remaining"
            ],
            datasets: [
                {
                    data: [<?php echo $cpu ?>, <?php echo 100 - $cpu; ?>],
                    backgroundColor: [
                        "#FF6384",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#FFCE56"
                    ]
                }]
        };

        var cpuChart = new Chart(ctx, {
            type: 'doughnut',
            data: data
        });

        var ctx = document.getElementById("ram-pie").getContext("2d");

        var data = {
            labels: [
                "Used",
                "Remaining"
            ],
            datasets: [
                {
                    data: [<?php echo $ram ?>, <?php echo $ram_max - $ram; ?>],
                    backgroundColor: [
                        "#FF6384",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#FFCE56"
                    ]
                }]
        };

        var ramChart = new Chart(ctx, {
            type: 'doughnut',
            data: data
        });

        var ctx = document.getElementById("storage-pie").getContext("2d");

        var data = {
            labels: [
                "Used",
                "Remaining"
            ],
            datasets: [
                {
                    data: [<?php echo $storage ?>, <?php echo $storage_max - $storage; ?>],
                    backgroundColor: [
                        "#FF6384",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#FFCE56"
                    ]
                }]
        };

        var cpuChart = new Chart(ctx, {
            type: 'doughnut',
            data: data
        });

    });

    function vnfStatus(vnf, status) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("config/vnf_status/" . $device_id); ?>",
            data: {
                vnf: vnf,
                status: status
            },
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status) {
                    $("#alertmessage").html(obj.alert);
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        $("#alertbox").modal('hide');
                        location.reload();
                    }, 1500);
                } else {
                    $("#alertmessage").html(obj.alert);
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        $("#alertbox").modal('hide');
                    }, 1500);
                }
            }, error: function (data) {
                alert("Update config failed");
            }
        });

    }

    function configAdd(form, modal) {
        if ($("#" + form).valid()) {
            var formdata = $("#" + form).serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("config/add_config/" . $device_id); ?>",
                data: formdata,
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status) {
                        $("#" + modal).modal('hide');
                        $("#alertmessage").html(obj.alert);
                        $("#alertbox").modal('show');
                        setTimeout(function () {
                            window.location.href = '<?php echo site_url($this->uri->segment(1) . '/device_detail/' . $this->uri->segment(3)) ?>';
                        }, 1500);
                    } else {
                        $("#" + modal).modal('hide');
                        $("#alertmessage").html(obj.alert);
                        $("#alertbox").modal('show');
                        setTimeout(function () {
                            $("#alertbox").modal('hide');
                            $("#" + modal).modal('show');
                        }, 1500);
                    }
                }, error: function (data) {
                    alert("Submit config failed");
                }
            });
        }
    }

    function configUpdate(form) {
        if ($("#" + form).valid()) {
            var formdata = $("#" + form).serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("config/update_config/" . $device_id); ?>",
                data: formdata,
                success: function (data) {
                    var obj = JSON.parse(data);
                    if (obj.status) {
                        $("#alertmessage").html(obj.alert);
                        $("#alertbox").modal('show');
                        setTimeout(function () {
                            $("#alertbox").modal('hide');
                        }, 1500);
                    } else {
                        $("#alertmessage").html(obj.alert);
                        $("#alertbox").modal('show');
                        setTimeout(function () {
                            $("#alertbox").modal('hide');
                        }, 1500);
                    }
                }, error: function (data) {
                    alert("Update config failed");
                }
            });
        }
    }

    function configDelete(service) {
        bootbox.confirm({
            message: "This action will delete the selected config. Are you sure?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result) {
                    tableData[tableData.length - 1] = service;
                    console.log(tableData);
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url("config/delete_config/" . $device_id); ?>",
                        data: {data: JSON.stringify(tableData)},
                        success: function (data) {
                            console.log(data);
                            var obj = JSON.parse(data);
                            if (obj.status) {
                                $("#alertmessage").html(obj.alert);
                                $("#alertbox").modal('show');
                                setTimeout(function () {
                                    window.location.href = '<?php echo site_url($this->uri->segment(1) . '/device_detail/' . $this->uri->segment(3)) ?>';
                                }, 1500);
                            } else {
                                $("#alertmessage").html(obj.alert);
                                $("#alertbox").modal('show');
                                setTimeout(function () {
                                    $("#alertbox").modal('hide');
                                }, 1500);
                            }
                        }, error: function (data) {
                            alert("Update config failed");
                        }
                    });
                }
            }
        }).find('.modal-content').css({
            'margin-top': '52%'
        });
    }
</script>
