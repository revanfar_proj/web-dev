<br/>
<div class="row">
    <ul class="nav navbar-top-links navbar-left">
        <a class="submenu" data-toggle="tab" href="#cust">Customer</a>
        <a class="submenu" data-toggle="tab" href="#dev">Device</a>
        <a class="submenu" data-toggle="tab" href="#ord">Order List</a>
    </ul>
    <br>
    <hr/>
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile overflow_hidden">
            <br>
            <div class="tab-content">
                <div id="ord" class="tab-pane fade">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel tile overflow_hidden">
                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                                <table class="table table-striped table-hover table-bordered" id="orderTables" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Phone Number</th>
                                            <th>Email</th>
                                            <th>Device Number</th>
                                            <th>Services</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $this->db->select('o.*');
                                        $this->db->from('orders o');
                                        $query = $this->db->get();
                                        $i = 0;
                                        if ($query->num_rows() > 0) {
                                            foreach ($query->result_array() as $row) {
                                                ?>
                                                <tr>
                                                    <td><?php echo ++$i ?></td>
                                                    <td><?php echo $row['name'] ?></td>
                                                    <td><?php echo $row['address'] ?></td>
                                                    <td><?php echo $row['phone'] ?></td>
                                                    <td><?php echo $row['email'] ?></td>
                                                    <td><?php echo $row['device'] ?></td>
                                                    <td><?php echo $row['services'] ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a class="modal-anchor" data-toggle="modal" data-target="#device_form" href="">+New Device</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="dev" class="tab-pane fade">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel tile overflow_hidden">
                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                                <table class="table table-striped table-hover table-bordered" id="deviceTables" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Hardware ID</th>
                                            <th>Serial Number</th>
                                            <th>Customer Name</th>
                                            <th>Site</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $this->db->select('c.name, d.*');
                                        $this->db->from('devices d');
                                        $this->db->join('customer c', 'c.customer_id = d.owner', 'left');
                                        $query = $this->db->get();
                                        if ($query->num_rows() > 0) {
                                            foreach ($query->result_array() as $row) {
                                                ?>
                                                <tr>
                                                    <td><a href="<?php echo site_url('admin/device_detail/' . $row['id']) ?>"><?php echo $row['id'] ?></a></td>
                                                    <td><?php echo $row['serial_number'] ?></td>
                                                    <td><?php echo $row['name'] ?></td>
                                                    <td><?php echo $row['site'] ?></td>
                                                    <td style="padding-top: 10px;"><a style="margin-right: 10px;" class="fa fa-remove pull-left"  onclick="deleteDevice('<?php echo $row['id'] ?>')" href="#"></a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a class="modal-anchor" data-toggle="modal" data-target="#device_form" href="">+New Device</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="cust" class="tab-pane fade in active">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel tile overflow_hidden">
                            <div class="col-md-12 col-sm-12 col-xs-12 table-responsive">
                                <table class="table table-striped table-hover table-bordered" id="customerTables" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="min-width: 110px">Customer ID</th>
                                            <th style="min-width: 130px">Customer Name</th>
                                            <th>Address</th>
                                            <!--<th style="min-width: 130px">Expired Date</th>-->
                                            <th style="min-width: 130px"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $this->db->select('c.*');
                                        $this->db->from('customer c');
                                        $query = $this->db->get();
                                        if ($query->num_rows() > 0) {
                                            foreach ($query->result_array() as $row) {
                                                ?>
                                                <tr>
                                                    <td><a href="<?php echo site_url('admin/customer_detail/' . $row['customer_id']) ?>"><?php echo $row['customer_id'] ?></a></td>
                                                    <td><?php echo $row['name'] ?></td>
                                                    <td><?php echo $row['address'] ?></td>
                                                    <!--<td>
                                                    <?php
                                                    //if (date("Y-m-d H:i:s", mktime(0, 0, 0)) < $row['expired_date']) {
                                                    //  echo 'Active';
                                                    //} else {
                                                    //    echo 'Suspended';
                                                    //}
                                                    ?>
                                                    </td>-->
                                                    <td><a style="margin-right: 10px;" class="fa fa-remove pull-left" onclick="deleteCustomer(<?php echo $row['customer_id'] ?>)" href="#"></a></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>

                                </table>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <a class="modal-anchor" data-toggle="modal" data-target="#cust_form" href="">+New Customer</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="cust_form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Register Form</h4>
            </div>
            <div class="modal-body">
                <form id="customer" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Name</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="name" name="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Address</label>
                        <div class="col-sm-8">
                            <textarea class="form-control" id="address" name="address"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Phone Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="phone" name="phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-8">
                            <a class="btn btn-default" onclick="addCustomer()">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="device_form" class="modal fade" role="dialog" style=" top: 30%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Device Register Form</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="device">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Serial Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="serial_number" name="serial_number">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-8">
                            <a class="btn btn-default" onclick="addDevice()">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="alertbox" class="modal fade" data-backdrop="static" style="display: none; top: 40%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="alertmessage" class="modal-title" style="text-align: center">Device Register Form</h5>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#customerTables').DataTable({
            responsive: true,
            columnDefs: [{
                    targets: 4,
                    orderable: false
                }]
        });

        $('#deviceTables').DataTable({
            responsive: true,
            columnDefs: [{
                    targets: 4,
                    orderable: false
                }]
        });
        
        $('#orderTables').DataTable({
            responsive: true,
        });
    });

    function addCustomer() {
        var formdata = $("#customer").serialize();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("admin/add_customer"); ?>",
            data: formdata,
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status) {
                    $("#cust_form").modal('hide');
                    $("#alertmessage").html('Register New Customer Success');
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        window.location.href = '<?php echo site_url('admin') ?>';
                    }, 1500);
                }
            }, error: function (data) {
                $("#cust_form").modal('hide');
                $("#alertmessage").html('Register New Customer Failed. Please Check Server Connection');
                $("#alertbox").modal('show');
                setTimeout(function () {
                    $("#alertbox").modal('hide');
                }, 1500);
                $("#cust_form").modal('show');
            }
        });
    }

    function deleteCustomer(cust_id) {
        bootbox.confirm({
            message: "This action will delete the selected customer. Are you sure?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result) {
                    var formdata = {
                        customer_id: cust_id
                    };
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url("admin/del_customer"); ?>",
                        data: formdata,
                        success: function (data) {
                            var obj = JSON.parse(data);
                            if (obj.status) {
                                $("#alertmessage").html('Delete Customer Success');
                                $("#alertbox").modal('show');
                                setTimeout(function () {
                                    window.location.href = '<?php echo site_url('admin') ?>';
                                }, 1500);
                            }
                        }, error: function (data) {
                            $("#alertmessage").html('Delete Customer Failed. Please Check Server Connection');
                            $("#alertbox").modal('show');
                            setTimeout(function () {
                                $("#alertbox").modal('hide');
                            }, 1500);
                            alert(data);
                        }
                    });
                }
            }
        }).find('.modal-content').css({
            'margin-top': '48%'
        });
    }

    function addDevice() {
        var formdata = $("#device").serialize();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("admin/reg_device"); ?>",
            data: formdata,
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status) {
                    $("#device_form").modal('hide');
                    $("#alertmessage").html('Register New Device Success');
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        window.location.href = '<?php echo site_url('admin') ?>';
                    }, 1500);
                } else {
                    $("#device_form").modal('hide');
                    $("#alertmessage").html('Register New Device Failed. Device with Serial Number ' + obj.serial_number + ' Already Exist');
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        $("#alertbox").modal('hide');
                    }, 1500);
                }
            }, error: function (data) {
                $("#device_form").modal('hide');
                $("#alertmessage").html('Register New Device Failed. Please Check Server Connection');
                $("#alertbox").modal('show');
                setTimeout(function () {
                    $("#alertbox").modal('hide');
                }, 1500);
            }
        });
    }

    function deleteDevice(id) {
        bootbox.confirm({
            message: "This action will delete the selected device. Are you sure?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-primary'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result) {
                    var formdata = {
                        hardware_id: id
                    };
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url("admin/del_device"); ?>",
                        data: formdata,
                        success: function (data) {
                            var obj = JSON.parse(data);
                            if (obj.status) {
                                $("#alertmessage").html('Delete Device Success');
                                $("#alertbox").modal('show');
                                setTimeout(function () {
                                    window.location.href = '<?php echo site_url('admin') ?>';
                                }, 1500);
                            }
                        }, error: function (data) {
                            $("#alertmessage").html('Register New Device Failed. Please Check Server Connection');
                            $("#alertbox").modal('show');
                            setTimeout(function () {
                                $("#alertbox").modal('hide');
                            }, 1500);
                        }
                    });
                }
            }
        }).find('.modal-content').css({
            'margin-top': '48%'
        });
    }
</script>
