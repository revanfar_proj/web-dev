<div class="row">
    <br/>
    <div class="col-lg-12">
        <div class="col-lg-12" style="padding-left: 0;">
            <div class="" id="net-graph" style="height: 150px;"></div>
            <div class="loader" id="loader"></div>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-lg-12 table-responsive">
        <table class="table table-warning table-bordered table-hover" id="deviceTables" style="width: 100%">
            <thead>
                <tr>
                    <th>Hardware ID</th>
                    <th>Serial Number</th>
                    <th>Site</th>
                    <th>vRouter Status</th>
                    <th>vFirewall Status</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $this->db->select('c.name, d.serial_number, d.id, d.site');
                $this->db->from('customer c, devices d');
                $this->db->where('c.customer_id = d.owner');
                $this->db->where('d.owner', $this->session->userdata("customer_id"));
                $query = $this->db->get();
                $config = array();
                if ($query->num_rows() > 0) {
                    foreach ($query->result_array() as $row) {
                        ?>
                        <tr>
                            <td><?php echo $row['id'] ?></td>
                            <td><?php echo $row['serial_number'] ?></td>
                            <td><?php echo $row['site'] ?></td>
                            <?php
                            $configpath = device_url() . $row['id'] . "/conf_example.json";
                            if (file_exists('data/vcpe/' . $row['id'] . '/conf_example.json')) {
                                $config = json_decode(file_get_contents($configpath), true);
                            }
                            ?>
                            <td class="vrouter_state"><?php if (array_key_exists('vRouter', $config)) { echo $config['vRouter']; } else { echo "Unknown"; }?></td>
                            <td class="vfirewall_state"><?php if (array_key_exists('vFirewall', $config)) { echo $config['vFirewall']; } else { echo "Unknown"; } ?></td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
    <a data-toggle="modal" href="#device_form"><i class="fa fa-plus"> Add Device</i></a>
</div>

<div id="device_form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Register Device</h4>
            </div>
            <div class="modal-body">
                <form id="device" class="form-horizontal" method="post" action="<?php echo site_url('customer/add_device') ?>">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Hardware ID</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="id" name="id">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Serial Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="serial_number" name="serial_number">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Site</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="site" name="site">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-8">
                            <a class="btn btn-default" id="deviceSubmit">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="alertbox" class="modal fade" data-backdrop="static" style="display: none; top: 40%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="alertmessage" class="modal-title" style="text-align: center">Device Register Form</h5>
            </div>
        </div>
    </div>
</div>
<script>
    var table = document.getElementById('deviceTables');
    var date = new Date;

    $(document).ready(function () {
        $('#deviceTables').DataTable({
            responsive: true

        });
        $("#loader").hide();
        graph(table.rows[1].cells[0].innerHTML);

        $('#device').validate({
            rules: {
                id: {
                    minlength: 8,
                    required: true
                },
                serial_number: {
                    minlength: 15,
                    required: true
                },
                site: {
                    minlength: 4,
                    required: true
                }
            },
            highlight: function (element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function (element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function (error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });


    });

    $('#deviceTables tbody tr').hover(function () {
        $('#net-graph').hide();
        $('#loader').show();
        $('#loader').hide();
        graph($(this).find('td:first-child').text());
        $('#net-graph').show();
    });

    $('#deviceTables tbody tr').click(function () {
        window.location.href = "<?php base_url() ?>customer/device_detail/" + $(this).find('td:first-child').text();
    });

    $('#deviceSubmit').click(function () {
        if ($("#device").valid()) {
            var formdata = $("#device").serialize();
            $.ajax({
                type: "POST",
                url: "<?php echo base_url("customer/add_device"); ?>",
                data: formdata,
                success: function (data) {
                    console.log(data);
                    var obj = JSON.parse(data);
                    if (obj.status) {
                        $("#device_form").modal('hide');
                        $("#alertmessage").html('Register New Device Success');
                        $("#alertbox").modal('show');
                        setTimeout(function () {
                            window.location.href = '<?php echo site_url('admin') ?>';
                        }, 1500);
                    }
                }, error: function (data) {
                    $("#device_form").modal('hide');
                    $("#alertmessage").html('Register New Device Failed. Please Check Server Connection');
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        $("#alertbox").modal('hide');
                    }, 1500);
                    $("#device_form").modal('show');
                }
            });
        }
    });

    function graph(id) {

        $.getJSON('<?php echo device_url(); ?>' + id + '/stats', function (json) {
            var recv = [];
            var sent = [];
            $.each(json, function (index, element) {
                var datetime = element.datetime;
                datetime = datetime.split("-");
                var time = datetime[1].match(/.{1,2}/g);
                var date = datetime[0].match(/.{1,4}/g);
                date[1] = date[1].match(/.{1,2}/g);
                if (typeof element.host.net.enp2s0 != "undefined") {
                    var data = [Date.UTC(date[0], parseInt(date[1][0]) - 1, date[1][1], time[0], time[1], 0), element.host.net.enp2s0.recv / 1000];
                    recv.push(data);
                    var data = [Date.UTC(date[0], parseInt(date[1][0]) - 1, date[1][1], time[0], time[1], 0), element.host.net.enp2s0.sent / 1000];
                    sent.push(data);
                }
            });
            var options = {
                yaxis: {
                    min: 0,
                    max: 100,
                    tickFormatter: function (v, axis) {
                        return v + " MB/s";
                    }
                },
                xaxis: {
                    mode: "time",
                    timezone: "Asia/Jakarta",
                    min: date.getTime() + 60 * 60000,
                    max: date.getTime() + 420 * 60000,
                    tickSize: [30, "minute"]
                },
                series: {
                    lines: {
                        show: true,
                        fill: true
                    }
                },
                clickable: true,
                hoverable: true,
                colors: ["#00a9e0", "#617bff"]
            };

            var plotObj = $.plot($("#net-graph"), [{
                    data: sent,
                    label: "Receive Bytes"
                }, {
                    data: recv,
                    label: "Sent Bytes"
                }],
                    options
                    );
        });
    }

    function vnfStats(id) {
        console.log('go');
        $.ajax({
            type: "GET",
            url: "<?php echo base_url("api/openvpn/status_sum/"); ?>" + id,
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.state) {
                    if (obj.vRouter) {
                        $("#vrouter_state").html("Online");
                    } else {
                        $("#vrouter_state").html("Offline");
                    }
                    if (obj.vFirewall) {
                        $("#vfirewall_state").html("Online");
                    } else {
                        $("#vfirewall_state").html("Offline");
                    }
                    $("#vcpe_state").html("Online");
                } else {
                    $("#vrouter_state").html('unknown');
                    $("#vfirewall_state").html('unknown');
                    $("#vcpe_state").html('vCPE (Offline)');
                }
            }, error: function (data) {
                alert("failed detect device management agent. device maybe offline or shutdown.");
            }
        });
    }
</script>