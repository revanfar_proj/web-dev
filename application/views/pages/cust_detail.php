<br/>
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel tile overflow_hidden">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="table-responsive">
                    <form id="customer">
                        <table class="table table-customer" style="width: auto;">
                            <tbody>
                                <?php
                                $this->db->select('c.*');
                                $this->db->from('customer c');
                                $this->db->where('customer_id', $this->uri->segment(3));
                                $this->db->limit(1);
                                $query = $this->db->get();

                                if ($query->num_rows() == 1) {
                                    foreach ($query->result_array() as $row) {
                                        ?>
                                        <tr>
                                            <td>Customer ID</td><td><input type="text" class="form-control" id="customer_id" name="customer_id" value="<?php echo $row["customer_id"] ?>" readonly/></td>
                                        </tr>
                                        <tr>
                                            <td>Customer Name</td><td><input type="text" class="form-control" id="name" name="name" value="<?php echo $row["name"] ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td>Address</td><td><textarea class="form-control" id="address" name="address" style="height: 120px"><?php echo $row["address"] ?></textarea></td>
                                        </tr>
                                        <tr>
                                            <td>Phone Number</td><td><input type="text" class="form-control" id="phone" name="phone" value="<?php echo $row["phone_number"] ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td><td><input type="text" class="form-control" id="email" name="email" value="<?php echo $row["email"] ?>"/></td>
                                        </tr>
                                        <tr>
                                            <td></td><td><a class="btn btn-primary pull-right" onclick="customerUpdate('customer')">Submit</a></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </form>
                </div>
                <hr/>
                <div class="col-lg-12 table-responsive">
                    <table class="table table-striped table-hover" id="customerTables">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Serial Number</th>
                                <th>Site</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $this->db->select('*');
                            $this->db->from('devices');
                            $this->db->where('owner', $this->uri->segment(3));
                            $query = $this->db->get();

                            if ($query->num_rows() > 0) {
                                foreach ($query->result_array() as $row) {
                                    ?>
                                    <tr>
                                        <td><a href="<?php echo site_url() . "admin/device_detail/" . $row['id'] ?>"><?php echo $row['id'] ?></a></td>
                                        <td><?php echo $row['serial_number'] ?></td>
                                        <td><?php echo $row['site'] ?></td>
                                        <td>Online</td>
                                        <td><a style="margin-right: 10px;" class="fa fa-remove pull-left" onclick="unassignDevice('<?php echo $row['id'] ?>')" href="#"></a></td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <a class="modal-anchor" data-toggle="modal" data-target="#device_form" href="">+Add Device</a>  
                    <a class="modal-anchor" href="<?php echo site_url('admin/get_device/' . $this->uri->segment(3)) ?>">+Get Device</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="device_form" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Customer Register Form</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="device">
                    <div class="form-group">
                        <label class="control-label col-sm-3">Hardware ID</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="id" name="id" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Serial Number</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="serial_number" name="serial_number" required>
                            <input type="hidden" id="owner" name="owner" value="<?php echo $this->uri->segment(3) ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-3">Site</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="site" name="site" required>
                            <input type="hidden" id="owner" name="owner" value="<?php echo $this->uri->segment(3) ?>">
                        </div>
                    </div>
                    <div class="form-group">        
                        <div class="col-sm-offset-3 col-sm-8">
                            <a class="btn btn-default" onclick="assignDevice()">Submit</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="alertbox" class="modal fade" data-backdrop="static" style="display: none; top: 40%;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="alertmessage" class="modal-title" style="text-align: center">Device Register Form</h5>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#customerTables').DataTable({
            responsive: true
        });
    });

    function customerUpdate(form) {
        var formdata = $("#" + form).serialize();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("admin/update_customer/") ?>",
            data: formdata,
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status) {
                    $("#alertmessage").html(obj.alert);
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        $("#alertbox").modal('hide');
                    }, 1500);
                } else {
                    $("#alertmessage").html(obj.alert);
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        $("#alertbox").modal('hide');
                    }, 1500);
                }
            }, error: function (data) {
                alert("Update Customer Data failed");
            }
        });
    }


    function assignDevice() {
        var formdata = $("#device").serialize();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("admin/assign_device"); ?>",
            data: formdata,
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status) {
                    $("#device_form").modal('hide');
                    $("#alertmessage").html('Assigning Device Success');
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        window.location.href = '<?php echo site_url('admin/customer_detail/' . $this->uri->segment(3)) ?>';
                    }, 1500);
                }
            }, error: function (data) {
                $("#device_form").modal('hide');
                $("#alertmessage").html('Assigning Device Failed. Please Check Server Connection');
                $("#alertbox").modal('show');
                setTimeout(function () {
                    $("#alertbox").modal('hide');
                }, 1500);
                $("#device_form").modal('show');
            }
        });
    }

    function unassignDevice(id) {
        var formdata = {
            hardware_id: id
        };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url("admin/unassign_device"); ?>",
            data: formdata,
            success: function (data) {
                var obj = JSON.parse(data);
                if (obj.status) {
                    $("#alertmessage").html('Delete Device Success');
                    $("#alertbox").modal('show');
                    setTimeout(function () {
                        window.location.href = '<?php echo site_url('admin/customer_detail/' . $this->uri->segment(3)) ?>';
                    }, 1500);
                }
            }, error: function (data) {
                $("#alertmessage").html('Register New Device Failed. Please Check Server Connection');
                $("#alertbox").modal('show');
                setTimeout(function () {
                    $("#alertbox").modal('hide');
                }, 1500);
            }
        });
    }
</script>