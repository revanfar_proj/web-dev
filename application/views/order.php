<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>vCPE | <?php echo ucfirst($title) ?></title>
        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" />
        <link href="<?php echo assets_url() ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/bootstrap-toggle/bootstrap-toggle.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/morrisjs/morris.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url() ?>vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url() ?>vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet" type="text/css">
        <link href="<?php echo assets_url() ?>dist/css/sb-admin-2.css" rel="stylesheet">
        <link href="<?php echo assets_url() ?>css/main.css" rel="stylesheet">
        <script src="<?php echo assets_url() ?>vendor/jquery/jquery.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/jquery-validation/dist/additional-methods.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootbox/bootbox.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/flot/jquery.flot.js"></script>
        <script src="<?php echo assets_url() ?>vendor/flot/jquery.flot.resize.js"></script>
        <script src="<?php echo assets_url() ?>vendor/flot/jquery.flot.time.js"></script>
        <script src="<?php echo assets_url() ?>vendor/datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/datatables-responsive/dataTables.responsive.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/metisMenu/metisMenu.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/raphael/raphael.min.js"></script>
        <script src="<?php echo assets_url() ?>vendor/bootstrap-toggle/bootstrap-toggle.min.js"></script>    
        <script src="<?php echo assets_url() ?>vendor/chart/Chart.min.js"></script>
    </head>
    <body style="background: white;">
        <div>
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php"><img src="<?php echo assets_url() ?>img/logo.png" height="20px"/></a>
                </div>
            </nav>
            <div class="row" style="width: 100%;">
                <div class="col-lg-offset-1 col-lg-10">
                    <h1>Order Device</h1>
                    <hr/>
                </div>
            </div>
            <div class="row" style="width: 100%;">
                <div class="col-lg-offset-1 col-lg-8">
                    <div class="col-lg-12">
                        <form id="order">
                            <div class="form-group">
                                <div class="col-lg-2" style="padding-top: 6px;"> 
                                    Name
                                </div>
                                <div class="col-lg-7"> 
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <div class="col-lg-2" style="padding-top: 6px;"> 
                                    Address
                                </div>
                                <div class="col-lg-7"> 
                                    <textarea class="form-control" id="address" name="address"></textarea>
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <br>
                                <div class="col-lg-2" style="padding-top: 6px;"> 
                                    Phone Number
                                </div>
                                <div class="col-lg-7"> 
                                    <input type="text" class="form-control" id="phone" name="phone">
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <div class="col-lg-2" style="padding-top: 6px;"> 
                                    Email
                                </div>
                                <div class="col-lg-7"> 
                                    <input type="text" class="form-control" id="email" name="email">
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <div class="col-lg-2" style="padding-top: 6px;"> 
                                    Device Number
                                </div>
                                <div class="col-lg-7"> 
                                    <input type="number" class="form-control" min="1" id="device" name="device" value="1">
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <div class="col-lg-2" style="padding-top: 6px;"> 
                                    Services
                                </div>
                                <div class="col-lg-3" style="margin-right: 20px"> 
                                    <select class="selectpicker" name="service[]" multiple>
                                        <optgroup label="Services">
                                            <option value="router">Router</option>
                                            <option value="firewall">Firewall</option>
                                            <option value="vpn">VPN</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-lg-1" style="padding-top: 6px;"> 
                                    Price
                                </div>
                                <div class="col-lg-3" style="margin-left: -20px;">  
                                    <input type="text" class="form-control" id="price" name="price" value="0" readonly>
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <div class="col-lg-offset-2 col-lg-7" style="padding-top: 6px; text-align: justify"> 
                                    <p>By clicking Create Order, you agree to our Terms and confirm that you have read our Data Policy, including our Cookie Use Policy. You may receive SMS message notifications from Facebook and can opt out at any time.</p>
                                </div>
                                <br>
                            </div>
                            <div class="form-group">
                                <br>
                                <div class="col-lg-offset-2 col-lg-7" style="padding-top: 6px;"> 
                                    <a class="btn btn-info pull-right" style="margin-right: 4px;" onclick="submit()">Create Order</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div id="alertbox" class="modal fade" data-backdrop="static" style="display: none; top: 40%;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="alertmessage" class="modal-title" style="text-align: center">Device Register Form</h5>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script src="<?php echo assets_url() ?>dist/js/sb-admin-2.js"></script>
    <script>
                                        $("#device").change(function () {
                                            price();
                                        });

                                        $(".selectpicker").change(function () {
                                            price();
                                        });

                                        function price() {
                                            var total = 0;
                                            var service = $('.selectpicker').val();
                                            if ($.inArray('router', service) >= 0) {
                                                total += 500000;
                                            }
                                            if ($.inArray('firewall', service) >= 0) {
                                                total += 700000;
                                            }
                                            if ($.inArray('vpn', service) >= 0) {
                                                total += 200000;
                                            }
                                            total *= $("#device").val();
                                            document.getElementById("price").value = total;
                                        }

                                        function submit() {
                                            var formdata = $("#order").serializeArray();
                                            console.log(formdata);
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url("main/input_order/"); ?>",
                                                data: formdata,
                                                success: function (data) {
                                                    var obj = JSON.parse(data);
                                                    if (obj.status) {
                                                        $("#alertmessage").html("Input order has been success.");
                                                        $("#alertbox").modal('show');
                                                        setTimeout(function () {
                                                            window.location.href = '<?php echo site_url('main/login') ?>';
                                                        }, 1500);
                                                    } else {
                                                        $("#alertmessage").html("Failed to input order.");
                                                        $("#alertbox").modal('show');
                                                        setTimeout(function () {
                                                            $("#alertbox").modal('hide');
                                                        }, 1500);
                                                    }
                                                }, error: function (data) {
                                                    alert("Failed to input order");
                                                }
                                            });
                                        }
    </script>
</html>
