<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer
 *
 * @author User
 */
class Config extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    public function vnf_status() {
        $filepath = device_url() . $this->uri->segment(3) . "/conf_example.json";
        $config = json_decode(file_get_contents($filepath), true);
        $status = $this->input->post("status");
        if($status == "true") {
            $config[$this->input->post("vnf")] = "on";
        } else {
            $config[$this->input->post("vnf")] = "off";
        }
        $config["version"] ++;
        $newJsonString = json_encode($config, JSON_PRETTY_PRINT);
        if (file_put_contents("/var/www/html/data/vcpe/" . $this->uri->segment(3) . "/conf_example.json", $newJsonString)) {
            $data["status"] = TRUE;
            $data["alert"] = $this->input->post("vnf"). " Status Has Been Changed. Please Wait 2 Minutes to Take Effects";
        } else {
            $data["status"] = FALSE;
            $data["alert"] = "Failed to Change ". $this->input->post("vnf"). " Status";
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function add_config() {
        $filepath = device_url() . $this->uri->segment(3) . "/conf_example.json";
        $config = json_decode(file_get_contents($filepath), true);
        if ($this->input->post("service") == "static") {
            $new_config["network"] = $this->input->post("network");
            $new_config["nexthop"] = $this->input->post("nexthop");
            if (!(in_array($new_config, $config["static"]))) {
                array_push($config["static"], $new_config);
            } else {
                $data["status"] = FALSE;
                $data["alert"] = "Route rules already exist";
                goto end;
            }
        }
        if ($this->input->post("service") == "ospf") {
            $new_config["network"] = $this->input->post("network");
            $new_config["area"] = $this->input->post("area");
            if (!(in_array($new_config, $config["ospf"]))) {
                array_push($config["ospf"], $new_config);
            } else {
                $data["status"] = FALSE;
                $data["alert"] = "Route rules already exist";
                goto end;
            }
        }
        if ($this->input->post("service") == "src_nat") {
            $new_config["rule"] = $this->input->post("rule");
            $new_config["src_address"] = $this->input->post("source_addr");
            $new_config["src_port"] = $this->input->post("source_addr");
            $new_config["dst_address"] = $this->input->post("destination_addr");
            $new_config["dst_port"] = $this->input->post("destination_port");
            $new_config["translate_address"] = $this->input->post("translation_addr");
            $new_config["translate_port"] = $this->input->post("translation_port");
            $exist = FALSE;
            foreach ($config["src_nat"] as $row) {
                if ($row["rule"] == $this->input->post("rule")) {
                    $exist = TRUE;
                }
            }
            if (!$exist) {
                array_push($config["src_nat"], $new_config);
            } else {
                $data["status"] = FALSE;
                $data["alert"] = "Source NAT rules already exist";
                goto end;
            }
        }
        if ($this->input->post("service") == "dst_nat") {
            $new_config["rule"] = $this->input->post("rule");
            $new_config["src_address"] = $this->input->post("source_addr");
            $new_config["src_port"] = $this->input->post("source_addr");
            $new_config["dst_address"] = $this->input->post("destination_addr");
            $new_config["dst_port"] = $this->input->post("destination_port");
            $new_config["translate_address"] = $this->input->post("translation_addr");
            $new_config["translate_port"] = $this->input->post("translation_port");
            $exist = FALSE;
            foreach ($config["dst_nat"] as $row) {
                if ($row["rule"] == $this->input->post("rule")) {
                    $exist = TRUE;
                }
            }
            if (!$exist) {
                array_push($config["dst_nat"], $new_config);
            } else {
                $data["status"] = FALSE;
                $data["alert"] = "Destination NAT rules already exist";
                goto end;
            }
        }
        $config["version"] ++;
        $newJsonString = json_encode($config, JSON_PRETTY_PRINT);
        if (file_put_contents("/var/www/html/data/vcpe/" . $this->uri->segment(3) . "/conf_example.json", $newJsonString)) {
            $data["status"] = TRUE;
            $data["alert"] = "Configuration Update Success";
        } else {
            $data["status"] = FALSE;
            $data["alert"] = "Failed to Update Configuration.";
        }
        end:
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function update_config() {
        $filepath = device_url() . $this->uri->segment(3) . "/conf_example.json";
        $config = json_decode(file_get_contents($filepath), true);
        if ($this->input->post("service") == "dns") {
            $config["dns"]["nameserver1"] = $this->input->post("name-server[0]");
            $config["dns"]["nameserver2"] = $this->input->post("name-server[1]");
        }
        if ($this->input->post("service") == "dhcp") {
            $config["dhcp"]["ip_subnet"] = $this->input->post("subnet");
            $config["dhcp"]["ip_start"] = $this->input->post("start");
            $config["dhcp"]["ip_stop"] = $this->input->post("stop");
            $config["dhcp"]["gateway"] = $this->input->post("gateway");
            $config["dhcp"]["lease"] = $this->input->post("lease");
        }
        if ($this->input->post("service") == "interface") {
            $config["wan_interface"] = $this->input->post("wan");
        }
        $config["version"] ++;
        $newJsonString = json_encode($config, JSON_PRETTY_PRINT);
        if (file_put_contents("/var/www/html/data/vcpe/" . $this->uri->segment(3) . "/conf_example.json", $newJsonString)) {
            $data["status"] = TRUE;
            $data["alert"] = strtoupper($this->input->post("service")) . " Has Been Updated";
        } else {
            $data["status"] = FALSE;
            $data["alert"] = "Failed to Update " . strtoupper($this->input->post("service")) . " Config";
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function delete_config() {
        $filepath = device_url() . $this->uri->segment(3) . "/conf_example.json";
        $config = json_decode(file_get_contents($filepath), true);
        $characters = array("[", "]", "\"");
        $data = explode(",", str_replace($characters, "", $this->input->post("data")));
        $service = $data[count($data) - 1];
        if ($service == "static") {
            $delete["network"] = $data[0];
            $delete["nexthop"] = $data[1];
        }
        if ($service == "ospf") {
            $delete["network"] = $data[0];
            $delete["area"] = $data[1];
        }
        if ($service == "src_nat") {
            $delete["rule"] = $data[0];
            $delete["src_address"] = $data[1];
            $delete["src_port"] = $data[2];
            $delete["dst_address"] = $data[3];
            $delete["dst_port"] = $data[4];
            $delete["translate_address"] = $data[5];
            $delete["translate_port"] = $data[6];
        }
        if ($service == "dst_nat") {
            $delete["rule"] = $data[0];
            $delete["src_address"] = $data[1];
            $delete["src_port"] = $data[2];
            $delete["dst_address"] = $data[3];
            $delete["dst_port"] = $data[4];
            $delete["translate_address"] = $data[5];
            $delete["translate_port"] = $data[6];
        }
        $new_array = Array();
        foreach ($config[$service] as $row) {
            if ($row != $delete) {
                array_push($new_array, $row);
            }
        }
        $config[$service] = $new_array;
        $config["version"] ++;
        $newJsonString = json_encode($config, JSON_PRETTY_PRINT);
        if (file_put_contents("/var/www/html/data/vcpe/" . $this->uri->segment(3) . "/conf_example.json", $newJsonString)) {
            $data["status"] = TRUE;
            $data["alert"] = "Config Has Been Removed";
        } else {
            $data["status"] = FALSE;
            $data["alert"] = "Failed to Remove Config";
        }
        end:
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

}
