<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    private $userdata;

    public function index() {
        if ($this->session->userdata('logged_in')) {
            if ($this->session->userdata('level') == "admin") {
                redirect('admin');
            } elseif ($this->session->userdata('level') == "customer") {
                redirect('customer');
            }
        } else {
            redirect('main/login');
        }
    }

    public function login() {
        $data['title'] = 'Login';
        $this->load->view('login', $data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect();
    }

    public function order() {
        $data['title'] = 'Order';
        $this->load->view('order', $data);
    }

    public function input_order() {
        $services = implode(',', $this->input->post("service"));
        $data = array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'phone' => $this->input->post('phone'),
            'email' => $this->input->post('email'),
            'device' => $this->input->post('device'),
            'services' => $services
        );
        if ($this->db->insert('orders', $data)) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function authentication() {
        if ($this->input->post('group') == 'customer') {
            $this->db->select('*');
            $this->db->from('customer');
            $this->db->where('customer_id', $this->input->post('username'));
            $this->db->where('password', MD5($this->input->post('password')));
            //$this->db->where('password', $this->input->post('password'));
            $this->db->limit(1);
            $query = $this->db->get();

            if ($query->num_rows() == 1) {
                foreach ($query->result() as $row) {
                    $userdata = array(
                        'customer_id' => $row->customer_id,
                        'name' => $row->name,
                        'address' => $row->address,
                        'membership' => $row->membership,
                        'expired_date' => $row->expired_date,
                        'level' => 'customer',
                        'logged_in' => TRUE
                    );
                    $this->session->set_userdata($userdata);
                }
            }
        } elseif ($this->input->post('group') == 'admin') {
            $this->db->select('username, password');
            $this->db->from('admin');
            $this->db->where('username', $this->input->post('username'));
            $this->db->where('password', MD5($this->input->post('password')));
            //$this->db->where('password', $this->input->post('password'));
            $this->db->limit(1);
            $query = $this->db->get();

            if ($query->num_rows() == 1) {
                $userdata = array(
                    'username' => $this->input->post('username'),
                    'level' => 'admin',
                    'logged_in' => TRUE
                );
                $this->session->set_userdata($userdata);
            }
        }

        redirect('');
    }

    public function test() {
        $data['title'] = 'test';
        $data['page'] = 'test';
        $this->load->view('index', $data);
    }

    public function vnfRemote($ip) {
        $this->load->library('reformatter');
        echo $this->reformatter->APIAccess($ip, $_POST);
    }

}
