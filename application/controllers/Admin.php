<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function login_check() {
        if ($this->session->userdata('level') !== 'admin' && $this->session->userdata('level')) {
            redirect('main');
        }
    }

    public function index() {
        $data['title'] = 'index';
        $data['page'] = 'admin_index';
        $this->load->view('index', $data);
    }

    public function customer_detail() {
        $data['title'] = 'Customer Detail';
        $data['page'] = 'cust_detail';
        $this->load->view('index', $data);
    }

    public function device_detail() {
        $data['title'] = 'Device Detail';
        $data['page'] = 'device_detail';
        $this->load->view('index', $data);
    }

    public function services() {
        if ($this->session->userdata('logged_in')) {
            $data['title'] = 'Service Templates';
            $data['page'] = 'service';
            $this->load->view('index', $data);
        } else {
            redirect('main/login');
        }
    }

    public function add_customer() {
        $data = array(
            'name' => $this->input->post('name'),
            'password' => md5($this->input->post('password')),
            'address' => $this->input->post('address'),
            'phone_number' => $this->input->post('phone'),
            'email' => $this->input->post('email')
        );
        if ($this->db->insert('customer', $data)) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
    
    public function update_customer() {
        $data = array(
            'name' => $this->input->post('name'),
            'address' => $this->input->post('address'),
            'phone_number' => $this->input->post('phone'),
            'email' => $this->input->post('email')
        );
        $this->db->where('customer_id', $this->input->post('customer_id'));
        if ($this->db->update('customer', $data)) {
            $data["status"] = true;
            $data["alert"] = "Data Updated";
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function del_customer() {
        $data = array(
            'owner' => null
        );
        $this->db->where('owner', $this->input->post('customer_id'));
        $this->db->update('devices', $data);
        $this->db->where('customer_id', $this->input->post('customer_id'));
        if ($this->db->delete('customer')) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function unassign_device() {
        $this->db->set('owner', 'NULL', false);
        $this->db->set('site', 'NULL', false);
        $this->db->where('id', $this->input->post('hardware_id'));
        if ($this->db->update('devices')) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function assign_device() {
        $this->db->select('*');
        $this->db->from('devices');
        $this->db->where('id', $this->input->post('id'));
        $this->db->where('serial_number', $this->input->post('serial_number'));
        $this->db->where('owner is null');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
                $this->db->set('id', $row['id']);
                $this->db->set('serial_number', $row['serial_number']);
                $this->db->set('site', $this->input->post('site'));
                $this->db->set('owner', $this->input->post('owner'));
                $this->db->set('salt', $row['salt']);
                $this->db->set('status', $row['status']);
            }
            if ($this->db->replace('devices')) {
                $data["status"] = true;
            } else {
                $data["status"] = false;
            }
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function reg_device() {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $id = '';
        for ($i = 0; $i <= 7; $i++) {
            $id .= $characters[rand(0, $charactersLength - 1)];
        }
        $this->db->where('serial_number', $this->input->post('serial_number'));
        $query = $this->db->get('devices');
        if ($query->num_rows() == 0) {
            a:
            $this->db->where('id', $id);
            $query = $this->db->get('devices');
            if ($query->num_rows() == 1) {
                goto a;
            } else {
                $data = array(
                    'id' => $id,
                    'serial_number' => $this->input->post('serial_number')
                );
                if ($this->db->insert('devices', $data)) {
                    $data["status"] = true;
                } else {
                    $data["status"] = false;
                }
            }
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function del_device() {
        $this->db->where('id', $this->input->post('hardware_id'));
        if ($this->db->delete('devices')) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function get_device() {
        $this->db->select('*');
        $this->db->from('devices');
        $this->db->where('owner', $this->uri->segment(3));
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            $filepath = '/var/www/html/data/vcpe/device.txt';
            if (file_exists($filepath)) {
                unlink($filepath);
            }
            foreach ($query->result_array() as $row) {
                $txt = $row['id'] . ':' . $row['serial_number'] . ':' . $row['status'];
                write_file($filepath, $txt . "\r\n", 'a');
            }
            force_download($filepath, null);
            unlink($filepath);
        }
    }   

}
