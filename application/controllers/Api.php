<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('file');
        $this->load->helper('date');
    }

    public function index() {
        echo 'sorry, wrong way';
    }

    public function token($vcpe_id) {
        #$data = json_decode(file_get_contents('php://input'), true);
        $second = floor(Date('s') / 10) * 10;
        $date = date("Y-n-j_G:i:s", mktime(date("H"), date("i"), $second, date('m'), date('d'), date('Y')));
        echo $date . "|" . $vcpe_id . ' - ';
        $token = md5($date . "|" . $vcpe_id);
        return $token;
        #echo "server_date: ".$date;
        #echo "\nserver_token: ".$token;
        #echo "\n<br>vcpe_token: " . $data['token'];
        
    }

    public function statistic($vcpe_id) {
        $data = json_decode(file_get_contents('php://input'), true);
        if (count($data) == 0) {
            echo 'failed to processing request';
        } else {
            echo $this->token($vcpe_id);
            echo ' - ';
            echo $data['token'];
            echo ' - ';

            if ($this->token($vcpe_id) != $data['token']) {
                echo 'Authentication failed';
                return False;
            }

            unset($data['token']);

            $dir = './data/vcpe/' . $vcpe_id . '/stats';
            $read = read_file($dir);
            if ($read) {
                $stats_old = json_decode($read, true);
                if (count($stats_old) >= 24) {
                    $stats_new = array_slice($stats_old, -23);
                } else {
                    $stats_new = $stats_old;
                }
            } else {
                $stats_new = array();
            }
            array_push($stats_new, $data);
            //print_r($stats_arr);
            if (write_file($dir, json_encode($stats_new, JSON_PRETTY_PRINT), 'w')) {
                echo '{"result":true}';
            } else {
                echo '{"result":false}';
            }
        }
    }

    public function status_sum($vcpe_id){
        $id = $vcpe_id;
        $state = $this->openvpn($id, true)['result'];
        if ($state==false) {
            $res[$id] = array(
                'state' => $state
                );
        } else {
            $vrouter = false;
            $vfirewall = false;
            if (file_exists("/var/www/html/data/vcpe/$id/stats")==1) {
                $dir = './data/vcpe/' . $vcpe_id . '/conf_example.json';
                $read = read_file($dir);
                $conf_arr = json_decode($read, true);
                var_dump($stat);
                $latest = array();
                if (count($stat) > 0) {
                    $latest = end($stat);
                    var_dump($latest);
                    if ($latest["vRouter"]["state"] == 1) {
                        $vrouter = true;
                    }
                    if ($latest["vFirewall"]["state"] == 1) {
                        $vfirewall = true;
                    }
                }
            }
            $res[$id] = array(
                'state'     => $state,
                'vRouter'   => $vrouter,
                'vFirewall' => $vfirewall);
        }                
        echo json_encode($res, JSON_PRETTY_PRINT);
    }

    public function status_sum2($cust_id){
        $this->db->select('id');
        $this->db->from('devices');
        $this->db->where('owner', $cust_id);
        $query = $this->db->get()->result_array();
        if (count($query)>0) {
            $res = array();
            foreach ($query as $value) {
                $id = $value['id'];
                $state = $this->openvpn($id, true)['result'];
                if ($state==false) {
                    $res[$id] = array(
                        'state' => $state
                        );
                } else {
                    $vrouter = false;
                    $vfirewall = false;
                    if (file_exists("data/vcpe/$id/stats")) {
                        $stat = json_decode("/var/www/html/data/vcpe/$id/stats", true);
                        $latest = array();
                        if (count($stat) > 0) {
                            $latest = end($stat);
                            if ($latest["vRouter"]["state"] == 1) {
                                $vrouter = true;
                            }
                            if ($latest["vFirewall"]["state"] == 1) {
                                $vfirewall = true;
                            }
                        }
                    }
                    $res[$id] = array(
                        'state'     => $state,
                        'vRouter'   => $vrouter,
                        'vFirewall' => $vfirewall);
                }                
            }
            echo json_encode($res, JSON_PRETTY_PRINT);
        } else {
            echo 'empty registered vcpe in this user';
        }
    }

    public function config($vcpe_id) {
        $data = json_decode(file_get_contents('php://input'), true);
        if (count($data) == 0) {
            echo 'failed to processing request';
        } else {

            if ($this->token($vcpe_id) != $data['token']) {
                echo 'Authentication failed';
                return;
            }

            unset($data['token']);

            $dir = './data/vcpe/' . $vcpe_id . '/conf_example.json';
            $read = read_file($dir);
            $conf_arr = json_decode($read, true);
            $version = $conf_arr['version'];
            if ($data['version'] == $version) {
                echo '{"result":true}';
            } else {
                echo '{"result":false, "conf":' . $read . '}';
            }
        }
    }

    public function openvpn($id, $intern = false) {
        $read = read_file('/var/run/openvpn-status.log');

        $arr = explode("\n", $read);
        $find = array();
        foreach ($arr as $val) {
            if (strpos($val, $id) !== false) {
                array_push($find, $val);
            }
        }

        if (count($find) == 0) {
            $res = array("result" => false);
        } else {
            $find0 = explode(',', $find[0]);
            $find1 = explode(',', $find[1]);
            $res = array(
                "result" => true,
                "hostname" => $find0[0],
                "vpn_ip" => $find1[0],
                "real_ip" => $find0[1],
                "bytes_recv" => (int) $find0[2],
                "bytes_send" => (int) $find0[3],
                "conn_start" => $this->reformat_date($find0[4]),
                "last_ref" => $this->reformat_date($find1[3])
            );
        }
        if ($intern) {
            return $res;
        } else {
            echo json_encode($res, JSON_PRETTY_PRINT);
        }
    }

    function reformat_date($input) {
        //Thu Mar 23 01:02:57 2017
        $date = DateTime::createFromFormat('D M d H:i:s Y', $input);
        return $date->format('Y-m-d H:i:s');
    }

}
