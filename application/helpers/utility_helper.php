<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

function assets_url() {
    return base_url() . 'assets/';
}

function device_url() {
    return base_url() . 'data/vcpe/';
}
