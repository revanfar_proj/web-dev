<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Customer
 *
 * @author User
 */
class Customer extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->login_check();
    }

    public function login_check() {
        if ($this->session->userdata('level') !== 'customer' && $this->session->userdata('level')) {
            redirect('main');
        }
    }

    public function index() {
        $data['title'] = 'index';
        $data['page'] = 'customer_index';
        $this->load->view('index', $data);
    }

    public function device_detail() {
        $this->db->where('id', $this->uri->segment(3));
        $this->db->where('owner', $this->session->userdata('customer_id'));
        $query = $this->db->get('devices');
        if ($query->num_rows() == 1) {
            $data['title'] = 'index';
            $data['page'] = 'device_detail';
            $this->load->view('index', $data);
        } else {
            redirect('customer');
        }
    }
}
